package websokets

import (
	"encoding/json"
	"time"
	"log"
	"github.com/go-redis/redis"
	"errors"
)

type redisChannelRepository struct {
	config      *RedisConfig
	redisClient *redis.Client
}

func (r *redisChannelRepository) redisPingOrFail() {
	_, err := r.redisClient.Ping().Result()

	if err != nil {
		log.Fatalln("Can't connect to Redis")
	}
}

func (r *redisChannelRepository) GetChannel(channelName string) (*Channel, error) {
	r.redisPingOrFail()

	redisResult := r.redisClient.Get(r.config.ChannelPrefix + channelName)

	if redisResult.Err() != nil {
		return nil, errors.New("GetChannel: Can't find channel " + channelName)
	}

	var channel Channel
	err := json.Unmarshal([]byte(redisResult.Val()), &channel)

	return &channel, err
}

func (r *redisChannelRepository) Listen(hub *Hub) error {
	pubSub := r.redisClient.PSubscribe(r.config.MessagePrefix + "*")

	pubSub.Receive()

	for {
		msg, _ := pubSub.ReceiveMessage()
		var message Message

		json.Unmarshal([]byte(msg.Payload), &message)

		hub.broadcast <- &message
	}
}

func (r *redisChannelRepository) Publish(message *Message) error {
	r.redisPingOrFail()
	body, err := json.Marshal(message)
	if err != nil {
		return err
	}

	r.redisClient.Publish(r.config.MessagePrefix+message.Channel.Name, body)
	return nil
}

func (r *redisChannelRepository) SaveChannel(channel *Channel) error {
	r.redisPingOrFail()

	body, err := json.Marshal(channel)

	if err != nil {
		return err
	}

	status := r.redisClient.Set(r.config.ChannelPrefix+channel.Name, string(body), time.Duration(86400*1000000000))
	if status.Err() != nil {
		log.Println(status.Err())
	}

	return nil
}

func (r *redisChannelRepository) GetAllChannels() []string {
	r.redisPingOrFail()

	var channels []string

	for _, channelName := range r.redisClient.Keys(r.config.ChannelPrefix + "*").Val() {
		channels = append(channels, channelName)
	}

	return channels
}

func newRedisChannelRepository(c *RedisConfig) *redisChannelRepository {

	return &redisChannelRepository{
		redisClient: redis.NewClient(&redis.Options{
			Addr:     c.ConnectionURI,
			Password: c.Password,
			DB:       c.Db,
		}),
		config: c,
	}
}
