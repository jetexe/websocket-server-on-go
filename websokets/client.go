package websokets

import (
	"bytes"
	"log"
	"net/http"
	"time"

	"github.com/gorilla/websocket"
	"encoding/json"
)

const (
	// Time allowed to write a message to the peer.
	writeWait = 10 * time.Second

	// Time allowed to read the next pong message from the peer.
	pongWait = 60 * time.Second

	// Send pings to peer with this period. Must be less than pongWait.
	pingPeriod = (pongWait * 9) / 10

	// Maximum message size allowed from peer.
	maxMessageSize = 512
)

var (
	newline = []byte{'\n'}
	space   = []byte{' '}
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

// Client is a middleman between the websocket connection and the HubInstance.
type Client struct {
	hub *Hub

	channels map[string]bool

	authKey string

	// The websocket connection.
	conn *websocket.Conn

	// Buffered channel of outbound messages.
	send chan *Message
}

// readPump pumps messages from the websocket connection to the HubInstance.
//
// The application runs readPump in a per-connection goroutine. The application
// ensures that there is at most one reader on a connection by executing all
// reads from this goroutine.
func (c *Client) readPump() {
	defer func() {
		c.hub.unregister <- c
		c.conn.Close()
	}()
	c.conn.SetReadLimit(maxMessageSize)
	c.conn.SetReadDeadline(time.Now().Add(pongWait))
	c.conn.SetPongHandler(func(string) error { c.conn.SetReadDeadline(time.Now().Add(pongWait)); return nil })
	for {
		_, message, err := c.conn.ReadMessage()
		if err != nil {
			if websocket.IsUnexpectedCloseError(err, websocket.CloseGoingAway, websocket.CloseAbnormalClosure) {
				log.Printf("error: %v", err)
			}
			break
		}
		message = bytes.TrimSpace(bytes.Replace(message, newline, space, -1))

		var command Command

		json.Unmarshal(message, &command)

		isError := false
		category := "success"

		switch command.Operation {
		case "subscribe":
			for _, channelName := range command.Channels {
				if err := c.subscribe(channelName); err != nil {
					isError = true
					category = err.Error()
				}
			}
		case "unsubscribe":
			for _, channelName := range command.Channels {
				c.unsubscribe(channelName)
			}
		default:
			isError = true
			category = "operationNotFound"
		}

		channels := make([]string, 0, len(c.channels))
		for k := range c.channels {
			channels = append(channels, k)
		}

		response := StatusMessage{Channels: channels, Operation: command.Operation, Category: category, IsError: isError}

		c.send <- &Message{Channel: &Channel{Name: "status", IsAuthed: false}, Payload: response}

	}
}

func (c *Client) subscribe(channelName string) error {
	channel, err := GetChannel(channelName)
	if err != nil {
		return err
	}
	c.channels[channel.Name] = true
	return nil
}

func (c *Client) unsubscribe(channelName string) {
	delete(c.channels, channelName)
}

// writePump pumps messages from the HubInstance to the websocket connection.
//
// A goroutine running writePump is started for each connection. The
// application ensures that there is at most one writer to a connection by
// executing all writes from this goroutine.
func (c *Client) writePump() {
	ticker := time.NewTicker(pingPeriod)
	defer func() {
		ticker.Stop()
		c.conn.Close()
	}()
	for {
		select {
		case message, ok := <-c.send:
			c.conn.SetWriteDeadline(time.Now().Add(writeWait))
			if !ok {
				// The HubInstance closed the channel.
				c.conn.WriteMessage(websocket.CloseMessage, []byte{})
				return
			}

			w, err := c.conn.NextWriter(websocket.TextMessage)
			if err != nil {
				return
			}
			if message == nil {
				log.Fatalln("Empty message")
			}

			if c == nil {
				log.Fatalln("Empty client")
			}

			if message.isNeedToWrite(c) {
				bodyJSON, err := message.toPublicJSON()
				if err == nil {
					w.Write(bodyJSON)
				} else {
					log.Println(err)
				}
			}
			if err := w.Close(); err != nil {
				return
			}
		case <-ticker.C:
			c.conn.SetWriteDeadline(time.Now().Add(writeWait))
			if err := c.conn.WriteMessage(websocket.PingMessage, nil); err != nil {
				return
			}
		}
	}
}

// serveWs handles websocket requests from the peer.
func ServeWs(w http.ResponseWriter, r *http.Request) {
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println(err)
		return
	}
	client := &Client{hub: HubInstance, conn: conn, send: make(chan *Message), channels: map[string]bool{"status": true}}
	client.hub.register <- client

	// Allow collection of memory referenced by the caller by doing all work in
	// new goroutines.
	go client.writePump()
	go client.readPump()
}
