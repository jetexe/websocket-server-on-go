package websokets

var ChannelsRepository ChannelsRepositoryInterface
var HubInstance *Hub

func Run(c *AppConfig) {
	ChannelsRepository = newChannelRepository(c)
	HubInstance = NewHub()

	go HubInstance.run()
	go ChannelsRepository.Listen(HubInstance)
}
