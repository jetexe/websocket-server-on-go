package websokets

import "encoding/json"

// Message struct
type Message struct {
	Channel *Channel    `json:"channel"`
	Payload interface{} `json:"payload"`
}

func (m *Message) toPublicJSON() ([]byte, error) {
	data := map[string]interface{}{
		"channel": m.Channel.Name,
		"payload": m.Payload,
	}

	return json.Marshal(data)
}

func (m *Message) isNeedToWrite(client *Client) bool {
	if m.Channel.Name == "status" {
		return true
	}

	isSubscribed := client.channels[m.Channel.Name]
	hasAuth := true

	channel, err := GetChannel(m.Channel.Name)

	if err != nil {
		return false
	}

	if channel.IsAuthed {
		hasAuth = channel.AuthKeys[client.authKey]
	}

	return isSubscribed && hasAuth
}
