package websokets

// StatusMessage that
type StatusMessage struct {
	IsError   bool     `json:"is_error"`
	Operation string   `json:"operation"`
	Category  string   `json:"category"`
	Channels  []string `json:"channels"`
}

// Command for some
type Command struct {
	Operation string   `json:"operation"`
	Channels  []string `json:"channels"`
}
