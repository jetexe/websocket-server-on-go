package websokets

import (
	"github.com/go-redis/redis"
	"log"
)

var redisClient *redis.Client

func initRedis(c *RedisConfig) {

	redisClient = redis.NewClient(&redis.Options{
		Addr:     c.ConnectionURI,
		Password: c.Password,
		DB:       c.Db,
	})
	redisPingOrFail()
}

func redisPingOrFail() {
	_, err := redisClient.Ping().Result()

	if err != nil {
		log.Fatalln("Can't connect to Redis")
	}
}
