package websokets

type ChannelsRepositoryInterface interface {
	GetChannel(channelName string) (*Channel, error)
	Listen(hub *Hub) error
	SaveChannel(channel *Channel) error
	GetAllChannels() []string
	Publish(message *Message) error
}

func newChannelRepository(c *AppConfig) ChannelsRepositoryInterface {
	if c.UseRedis {
		return newRedisChannelRepository(c.Redis)
	}
	return &insideChannelRepository{channels: make(map[string]*Channel, 0)}
}
