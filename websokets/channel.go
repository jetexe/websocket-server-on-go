package websokets

import (
	"errors"
)

// Channel description
type Channel struct {
	Name     string          `json:"name"`
	IsAuthed bool            `json:"is_authed"`
	AuthKeys map[string]bool `json:"auth_keys,omitempty"`
}

func GetChannel(channelName string) (*Channel, error) {
	if len(channelName) == 0 {
		return nil, errors.New("GetChannel: Channel name must be set")
	}

	return ChannelsRepository.GetChannel(channelName)
}

func (c *Channel) Save() error {
	return ChannelsRepository.SaveChannel(c)
}
