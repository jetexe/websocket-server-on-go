package websokets

// AppConfig struct for application
type AppConfig struct {
	UseRedis bool
	Redis    *RedisConfig
}

type RedisConfig struct {
	ConnectionURI string
	Password      string
	Db            int
	ChannelPrefix string
	MessagePrefix string
}
