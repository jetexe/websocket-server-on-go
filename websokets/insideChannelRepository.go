package websokets

import (
	"errors"
)

type insideChannelRepository struct {
	channels map[string]*Channel
	hub      *Hub
}

func (i *insideChannelRepository) GetChannel(channelName string) (*Channel, error) {
	channel, ok := i.channels[channelName]
	if !ok {
		return nil, errors.New("GetChannel: chanel " + channelName + " not found")
	}
	return channel, nil
}

func (i *insideChannelRepository) Listen(hub *Hub) error {
	i.hub = hub
	return nil
}

func (i *insideChannelRepository) SaveChannel(channel *Channel) error {
	i.channels[channel.Name] = channel

	return nil
}

func (i *insideChannelRepository) Publish(message *Message) error {
	i.hub.broadcast <- message
	return nil
}

func (i *insideChannelRepository) GetAllChannels() []string {

	var channels []string

	for channelName := range i.channels {
		channels = append(channels, channelName)
	}

	return channels
}
