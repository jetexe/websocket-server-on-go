package main

import (
	"gitlab.com/jetexe/websocket-server-on-go/websokets"
	"log"
	"strconv"
	"os"
)

func newConfigFromEnv() *websokets.AppConfig {

	config := &websokets.AppConfig{}

	if useRedis := getEnv("USE_REDIS", "false"); useRedis == "true" || useRedis == "1" {
		config.UseRedis = true
	}

	if config.UseRedis {
		redisDB, err := strconv.Atoi(getEnv("REDIS_DB", "1"))

		if err != nil {
			log.Fatalln("redis database must be integer")
		}

		redisConfig := &websokets.RedisConfig{
			ConnectionURI: getEnv("REDIS_HOST", "localhost") + ":" + getEnv("REDIS_PORT", "6379"),
			Password:      getEnv("REDIS_PASSWORD", ""),
			Db:            redisDB,
		}

		config.Redis = redisConfig
	}
	return config
}

func getEnv(key, fallback string) string {
	value := os.Getenv(key)
	if len(value) == 0 {
		return fallback
	}
	return value
}
