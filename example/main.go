package main

import (
	"log"
	"net/http"
	"gitlab.com/jetexe/websocket-server-on-go/websokets"
	"encoding/json"
)

func serveHome(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path != "/" {
		http.Error(w, "Not found", http.StatusNotFound)
		return
	}
	if r.Method != "GET" {
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
		return
	}
	http.ServeFile(w, r, "static/home.html")
}

func serveApi(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path != "/put" {
		http.Error(w, "Not found", http.StatusNotFound)
		return
	}
	if r.Method != "POST" {
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
		return
	}

	var m websokets.Message
	json.NewDecoder(r.Body).Decode(&m)

	websokets.ChannelsRepository.Publish(&m)
}

func main() {
	websokets.Run(newConfigFromEnv())
	ch := websokets.Channel{Name: "one", IsAuthed: false}
	ch.Save()
	http.HandleFunc("/", serveHome)
	http.HandleFunc("/put", serveApi)

	http.HandleFunc("/ws", func(w http.ResponseWriter, r *http.Request) {
		websokets.ServeWs(w, r)
	})
	err := http.ListenAndServe(":8008", nil)
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}
