package roadrunner

import (
	rrhttp "github.com/spiral/roadrunner/service/http"
	rr "github.com/spiral/roadrunner/cmd/rr/cmd"
	"net/http"
	"gitlab.com/jetexe/websocket-server-on-go/websokets"
	"github.com/sirupsen/logrus"
)

const ID = "websoket"

type Service struct {
	logger *logrus.Logger
}

func (s *Service) Init(r *rrhttp.Service) (ok bool, err error) {
	s.logger = rr.Logger
	websokets.Run(&websokets.AppConfig{
		UseRedis: false,
	})
	ch := websokets.Channel{Name: "one", IsAuthed: false}
	ch.Save()
	r.AddMiddleware(s.middleware)

	return true, nil
}

func (s *Service) middleware(f http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if !s.serveSocket(w, r) {
			f(w, r)
		}
	}
}

func (s *Service) serveSocket(w http.ResponseWriter, r *http.Request) bool {
	if r.RequestURI == "/ws" {
		s.logger.Debugln("Try to upgrade ws")
		websokets.ServeWs(w, r)
		return true
	}
	return false
}
